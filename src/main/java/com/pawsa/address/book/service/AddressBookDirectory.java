package com.pawsa.address.book.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AddressBookDirectory implements IAddressBookDirectory {

    @Autowired
    private Trie trie;

    /**
     * Insert name with respect to id into trie data structure
     * @param name
     * @param id
     */
    @Override
    public void insertIdNameIntoTrie(String name, String id) {
        trie.insert(name,id);
    }

    /**
     * Read id by name from trie data structure
     * @param name
     * @return
     */
    @Override
    public String getIdFromTrieUsingPersonName(String name) {
        return  trie.getIdByName(name);
    }
}
