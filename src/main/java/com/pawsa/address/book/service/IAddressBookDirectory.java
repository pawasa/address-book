package com.pawsa.address.book.service;

public interface IAddressBookDirectory {

    public void insertIdNameIntoTrie(String name, String id);
    public String getIdFromTrieUsingPersonName(String name);
}
