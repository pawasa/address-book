package com.pawsa.address.book.service;

import com.pawsa.address.book.model.Person;

public interface IAddressBookService {

    public void insertAddress(Person person);

    public Person searchPersonAddressByName(String name);

    public void deletePersonAddressByName(String name);

}
