package com.pawsa.address.book.service;

import com.pawsa.address.book.model.Person;
import com.pawsa.address.book.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class AddressBookServiceImpl implements IAddressBookService {

    @Autowired
    private AddressBookDirectory addressBookDirectory;

    @Autowired
    private PersonRepository personRepository;

    /**
     * Logic:
     * Step 1: Create unique id for every new record so bases on it we can do CRUD operations
     * Step 2: Store name and id into trie data structure.
     * Step 3: save record into database
     *
     * @param person
     */
    @Override
    public void insertAddress(Person person) {
        String id= UUID.randomUUID().toString();
        person.setId(id);
        addressBookDirectory.insertIdNameIntoTrie(person.getFirstName(),id);
        personRepository.savePerson(person);
    }

    /**
     * Logic:
     * Step 1: identify id by using name from trie data structure
     * Step 2: read record from database by id
     * @param name
     * @return
     */
    @Override
    public Person searchPersonAddressByName(String name) {
        String id=addressBookDirectory.getIdFromTrieUsingPersonName(name);
        return personRepository.searchPersonById(id);

    }

    /**
     * Logic:
     * Step 1: identify id by using name from trie data structure
     * Step 2: de;ete record from database by id
     * @param name
     * @return
     */
    @Override
    public void deletePersonAddressByName(String name) {
        String id=addressBookDirectory.getIdFromTrieUsingPersonName(name);
        personRepository.deletePerson(id);
    }
}
