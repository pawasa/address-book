package com.pawsa.address.book.model;

public class Contact {
    private ContactType contactType;
    private String number;

    public Contact() {

    }

    public Contact(ContactType contactType, String number) {
        this.contactType = contactType;
        this.number = number;
    }

    public ContactType getContactType() {
        return contactType;
    }

    public void setContactType(ContactType contactType) {
        this.contactType = contactType;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "contactType=" + contactType +
                ", number='" + number + '\'' +
                '}';
    }
}
