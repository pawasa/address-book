package com.pawsa.address.book.model;

public enum ContactType {
    MOBILE,WORK,HOME,OTHER
}
