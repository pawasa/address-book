package com.pawsa.address.book.model;

public enum AddressType {
    HOME, WORK
}
