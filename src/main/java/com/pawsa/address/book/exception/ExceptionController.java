package com.pawsa.address.book.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(value = AddressNotFoundException.class)
    public ResponseEntity<Object> addressNotFond(AddressNotFoundException addressNotFoundException) {
        return new ResponseEntity<>(addressNotFoundException.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    //Todo: Similarly we can apply global exception handling stratergy for other exceptions
}
