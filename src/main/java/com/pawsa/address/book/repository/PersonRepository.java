package com.pawsa.address.book.repository;

import com.pawsa.address.book.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PersonRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    public void savePerson(Person person) {
        mongoTemplate.save(person);
    }

    public Person searchPersonById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        List<Person> person = mongoTemplate.find(query, Person.class);
        return person.isEmpty() ? null : person.get(0);
    }

    public void deletePerson(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        mongoTemplate.remove(query, Person.class);
    }
}
