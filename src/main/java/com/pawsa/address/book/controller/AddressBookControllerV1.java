package com.pawsa.address.book.controller;

import com.pawsa.address.book.exception.AddressNotFoundException;
import com.pawsa.address.book.model.Person;
import com.pawsa.address.book.service.AddressBookServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/addressbook")
public class AddressBookControllerV1 {

    @Autowired
    private AddressBookServiceImpl addressBookService;

    @PostMapping("/person/add")
    public ResponseEntity<String> addPersonAddress(@RequestBody Person person) {
        addressBookService.insertAddress(person);
        return new ResponseEntity<>("Person Details Added successfully", new HttpHeaders(), HttpStatus.CREATED);
    }

    @GetMapping("/person/name/{firstName}")
    public ResponseEntity<Person> searchPersonAddress(@PathVariable("firstName") String firstName) {
        Person person  = addressBookService.searchPersonAddressByName(firstName);
        if(null==person) throw new AddressNotFoundException("For given name person/address not exist");
        return new ResponseEntity<>(person, new HttpHeaders(), HttpStatus.FOUND);
    }

    @PutMapping("/person/update")
    public ResponseEntity<String> updatePersonAddress(@RequestBody Person person) {
        addressBookService.insertAddress(person);
        return new ResponseEntity<>("Person Details Added successfully", new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping("/person/person/{firstName}")
    public ResponseEntity<String> updatePersonAddress(@PathVariable("firstName") String firstName) {
        addressBookService.deletePersonAddressByName(firstName);
        return new ResponseEntity<>("Person Details Added successfully", new HttpHeaders(), HttpStatus.OK);
    }


}
